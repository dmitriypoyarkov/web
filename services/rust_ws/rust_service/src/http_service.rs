use std::{
    io::prelude::*,
    net::{TcpListener, TcpStream},
    error::Error, str::from_utf8, time::Duration,
};

use kafka::producer::{Producer, RequiredAcks, Record};
use regex::Regex;
use redis::{AsyncCommands};
use redis::aio::Connection;

#[tokio::main]
async fn main() -> Result<(), Box<dyn Error>>{
    let listen_addr = std::env::args().nth(1).unwrap_or("0.0.0.0:7878".to_owned());
    let listener = TcpListener::bind(listen_addr).unwrap();

    // Block forever, handling each request that arrives at this IP address
    for stream in listener.incoming() {
        let stream = stream.unwrap();

        handle_connection(stream).await.unwrap();
    }
    Ok(())
}

async fn handle_connection(mut stream: TcpStream) -> Result<(), Box<dyn Error>>{
    let client = redis::Client::open("redis://redis:6379",)?;
    let mut con = client.get_tokio_connection().await?;
    inc_request_count(&mut con).await?;
    
    let mut buffer = [0; 1024];
    stream.read(&mut buffer).unwrap();
    let task_regex =
    Regex::new(r"GET /task/([0-9]+) HTTP/1\.1\r\n").unwrap();
    let caps = task_regex.captures(from_utf8(&buffer).unwrap()).unwrap();
    
    let task_no_str = caps.get(1).map_or("", |m| m.as_str());
    match task_no_str.parse::<u8>() {
        Ok(num) => {
            respond(stream, handle_task(&mut con, num).await.unwrap()).await;
            return Ok(());
        },
        Err(_) => {
            respond(stream, error_response(
                String::from("Request format: hostname:port/task/<task_no>!")
            ).await.unwrap()).await;
            return Ok(());
        },
    };
}

async fn respond(mut stream: TcpStream, resp_text : String) {
    stream.write_all(resp_text.as_bytes()).unwrap();
    stream.flush().unwrap();
}

async fn handle_task(con: &mut Connection, task_no: u8) -> Result<String, Box<dyn Error>> {
    // First: check if task exists
    let status_line = "HTTP/1.1 200 OK\r\n\r\n";
    
    
    
    let task_status : Option<String> = con.get(format!("task{task_no}")).await?;
    if !task_status.is_none() {
        let task_status_str = task_status.unwrap();
        if task_status_str == "done" {
            let task_result: String = con.get(format!("task{task_no}_result")).await?;
            let response: String = format!("{status_line}Task status: done\nResult: {task_result}\n");
            return Ok(response);
        }
        let response: String = format!("{status_line}Task status: {task_status_str}\n");
        return Ok(response);
    }

    let mut producer =
        Producer::from_hosts(vec!("broker:29092".to_owned()))
            .with_ack_timeout(Duration::from_secs(1))
            .with_required_acks(RequiredAcks::One)
            .create()
            .unwrap();
    con.set(format!("task{task_no}"), "in_queue").await?;
    producer.send(&Record::from_value("task-topic", vec![task_no])).unwrap();
    let response: String = format!("{status_line}Task status: in_queue\n");
    Ok(response)
}

async fn error_response(msg : String) -> Result<String, Box<dyn Error>> {
    let status_line = "HTTP/1.1 200 OK\r\n\r\n";
    let response = format!("{status_line}{msg}");
    Ok(response)
}

async fn inc_request_count(con : &mut Connection) -> Result<(), Box<dyn Error>>{
    let cur_req_count : Option<String> = con.get("req_count").await?;
    if cur_req_count.is_none() {
        con.set("req_count", "1").await?;
        return Ok(());
    }
    con.set("req_count",
        cur_req_count.unwrap().parse::<u32>().unwrap() + 1).await?;
    Ok(())
}