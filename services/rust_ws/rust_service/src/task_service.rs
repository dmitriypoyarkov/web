use kafka::consumer::{Consumer, FetchOffset, GroupOffsetStorage};
use redis::Commands;
use core::time;
use std::thread::sleep;

fn main() {
    // let client = redis::Client::open("redis://172.18.0.3:6379").unwrap();
    // let mut _con = client.get_connection().unwrap();
    let broker_addr = std::env::args().nth(1).unwrap_or("broker:29092".to_owned());
    let mut consumer =
    Consumer::from_hosts(vec!(broker_addr))
      .with_topic_partitions("task-topic".to_owned(), &[0])
      .create()
      .unwrap();
    loop {
        for ms in consumer.poll().unwrap().iter() {
          for m in ms.messages() {
            let task_no = m.value[0];
            let client = redis::Client::open("redis://redis:6379").unwrap();
            let mut con = client.get_connection().unwrap();
            let _: String = con.set(format!("task{task_no}"), "in_progress").unwrap();
            sleep(time::Duration::from_secs(12));
            let _: String = con.set(format!("task{task_no}"), "done").unwrap();
            let _: String = con.set(format!("task{task_no}_result"), format!("result{task_no}")).unwrap();
          }
          consumer.consume_messageset(ms).unwrap();
        }
        consumer.commit_consumed().unwrap();
      }
 }