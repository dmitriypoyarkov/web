using System.Diagnostics;

public enum CircuitState
{
    Up  =  0,
    Down = 1,
    HalfUp =  2
}
public class CircuitBreaker<T>
{
    private CircuitState state = CircuitState.Up;
    public CircuitState State {
        get => state;
        private set {
            if (value == state) return;
            state = value;
            this.logger?.LogInformation(
                $"Circuit state changed: {state.ToString()}");
        }
    }
    private Func<CancellationToken, Task<T>> connectionFunc;
    private int retriesFromUp;
    private int retriesFromHalfUp;
    private int upRetryTimeout;
    private int downRetryTimeout;
    private int maxAttemptsToUpState;
    private int attemptsToUpState;
    private ILogger? logger;

    public CircuitBreaker(
        Func<CancellationToken, Task<T>> connectionFunc,
        int retriesFromUp = 2,
        int retriesFromHalfUp = 1,
        int upRetryTimeout = 2000,
        int downRetryTimeout = 10000,
        int maxAttemptsToUpState = 2,
        ILogger? logger = null)
        {
            this.connectionFunc = connectionFunc;

            this.retriesFromUp = retriesFromUp;
            this.retriesFromHalfUp = retriesFromHalfUp;
            
            this.upRetryTimeout = upRetryTimeout;
            this.downRetryTimeout = downRetryTimeout;
            this.maxAttemptsToUpState = 2;

            this.attemptsToUpState = 0;

            this.logger = logger;
        }

    public async Task<T> WrapConnection(CancellationToken cancelToken)
    {
        if (this.State == CircuitState.Up)
        {
            for (int i = 0; i < this.retriesFromUp; i++)
            {
                try {
                    return await connectionFunc.Invoke(cancelToken);
                } catch (OperationCanceledException e) {
                    throw e;
                } catch {
                    logger?.LogWarning(
                        $"{typeof(T).ToString()}: Retry {i + 1} to connect failed");
                    await Task.Delay(this.upRetryTimeout, cancelToken);
                    continue;
                }
            }
        }
        else if (this.State == CircuitState.HalfUp)
        {
            for (int i = 0; i < this.retriesFromHalfUp; i++)
            {
                try {
                    var result = await connectionFunc.Invoke(cancelToken);
                    this.attemptsToUpState -= 1;
                    if (this.attemptsToUpState <= 0)
                    {
                        this.State = CircuitState.Up;
                    }
                    return result;
                } catch {
                    logger?.LogWarning(
                        $"{typeof(T).ToString()}: Retry {i + 1} to connect from HalfUp failed");
                    this.attemptsToUpState = this.maxAttemptsToUpState;
                    await Task.Delay(this.upRetryTimeout, cancelToken);
                    continue;
                }
            }
        }
        
        // retries passed
        this.State = CircuitState.Down;
        while(true)
        {
            await Task.Delay(this.downRetryTimeout, cancelToken);
            try {
                var result = await connectionFunc.Invoke(cancelToken);
                this.State = CircuitState.HalfUp;
                this.attemptsToUpState = this.maxAttemptsToUpState;
                return result;
            } catch {
                logger?.LogWarning(
                    $"{typeof(T).ToString()}: Retry to connect from Down failed");
                continue;
            }
        }
    }
}