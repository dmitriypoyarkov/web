using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ServiceStack.Redis;
using Confluent.Kafka;
using System.Net;

namespace TaskApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TaskItemsController : ControllerBase
    {
        private RedisManagerPool redisPool;
        private CircuitBreaker<IRedisClientAsync> redisCircuitBreaker;
        private ProducerConfig kafkaConfig;
        private ILogger logger;
        public TaskItemsController(IConfiguration configuration,
            ILogger<TaskItemsController> logger)
        {
            this.redisPool = new RedisManagerPool(configuration.GetConnectionString("Redis"));
            this.redisCircuitBreaker = new CircuitBreaker<IRedisClientAsync>(
                async (CancellationToken cancelToken) => await redisPool.GetClientAsync(cancelToken),
                logger: logger
            );
            this.kafkaConfig = new ProducerConfig {
                BootstrapServers = configuration.GetConnectionString("Kafka"),
                ClientId = Dns.GetHostName()
            };
            this.logger = logger;
        }
        // GET: api/TaskItems/5
        [HttpGet("{id}")]
        public async Task<ObjectResult> GetTaskItem(string id,
            CancellationToken cancelToken)
        {
            try 
            {
                this.logger.LogInformation($"Requested task {id}");
                var client = await this.redisCircuitBreaker.WrapConnection(cancelToken);
                string? taskStatus = await client.GetAsync<string>(id, cancelToken);
                if (taskStatus is not null)
                {
                    this.logger.LogInformation(
                        $"Task {id} exists, status: {taskStatus.ToString()}");
                    return Ok(taskStatus);
                }
                try {
                    using(var producer = new ProducerBuilder
                        <Null, string> (this.kafkaConfig).Build()) {
                        var result = await producer.ProduceAsync(
                            "task-topic",
                            new Message <Null, string> {
                                Value = id
                            }
                        );
                    }
                } catch (Exception ex) {
                    this.logger.LogError($"Error occured: {ex.Message}");
                }
                await client.SetAsync<string>(id, "in_queue");
                this.logger.LogInformation($"New task {id} added");
                return Ok("in_queue");
            } catch (OperationCanceledException) {
                logger.LogInformation("Request cancelled");
                return Ok("Operation was cancelled");
            }
        }
    }
}
