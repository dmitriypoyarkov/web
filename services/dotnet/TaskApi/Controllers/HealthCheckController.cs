using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ServiceStack.Redis;
using Confluent.Kafka;
using System.Net;

namespace TaskApi.Controllers
{
    [Route("[controller]/")]
    [ApiController]
    public class HealthcheckController : ControllerBase
    {
        private ProducerConfig kafkaConfig;
        private ILogger logger;
        public HealthcheckController(IConfiguration configuration,
            ILogger<HealthcheckController> logger)
        {
            this.kafkaConfig = new ProducerConfig {
                BootstrapServers = configuration.GetConnectionString("Kafka"),
                ClientId = Dns.GetHostName(),
                MessageTimeoutMs = 0
            };
            this.logger = logger;
            logger.LogInformation("Healthcheck initialized!");
        }

        [HttpGet]
        public ObjectResult GetHealthcheck()
        {
            logger.LogInformation("Healthcheck requested");
            string? errorMessage = null;
            var producer = new ProducerBuilder<Null, string>(
                this.kafkaConfig
            ).SetErrorHandler((_, error) =>
                {
                    logger.LogError($"Kafka error: {error.ToString()}");
                    errorMessage = error.ToString();
                }
            ).SetLogHandler((_, log) =>
                {
                    logger.LogError($"Kafka log (not asked for): {log.Message}");
                    errorMessage = log.Message;
                }
            )
            .Build();
            Thread.Sleep(500);
            if (errorMessage is not null)
            {
                logger.LogError("Healthcheck: not healthy!");
                return Ok($"Not healthy: {errorMessage}");
            }
            logger.LogInformation("Healthcheck: healthy");
            return Ok("Healthy");
        }
    }
}
