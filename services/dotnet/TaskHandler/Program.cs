var builder = WebApplication.CreateBuilder(args);

builder.Services.AddSingleton
    <IHostedService, TaskHandlerConsumer>();
builder.Services.AddControllers();

var app = builder.Build();


// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Error");
}
app.UseStaticFiles();

app.UseRouting();

app.UseAuthorization();

app.Run();
