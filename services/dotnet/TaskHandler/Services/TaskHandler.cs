using Confluent.Kafka;
using ServiceStack.Redis;

public class TaskHandlerConsumer : IHostedService
{
    private IConfiguration configuration;
    private ILogger logger;
    public TaskHandlerConsumer(IConfiguration configuration,
        ILogger logger)
    {
        this.configuration = configuration;
        this.logger = logger;
    }
    public async Task StartAsync(CancellationToken cancelToken)
    {
        Console.WriteLine("Task Handler Service: Started");
        var config = new ConsumerConfig
        {
            GroupId = "task-group",
            BootstrapServers = configuration.GetConnectionString("Kafka")
        };

        using (var consumerBuilder = new ConsumerBuilder
            <Ignore, string>(config).Build())
        using (var manager = new RedisManagerPool(
            configuration.GetConnectionString("Redis")))
        {
            consumerBuilder.Subscribe("task-topic");
            Console.WriteLine("Task Handler Service: Subscribed to topic");
            
            Console.WriteLine("Task Handler Service: Redis Client Acquired");
            try
            {
                while (true)
                {
                    var redisClient = await manager.GetClientAsync();
                    Console.WriteLine("Task Handler Service: Start Consuming");
                    var consumer = consumerBuilder.Consume(cancelToken);
                    var taskId = consumer.Message.Value;
                    Console.WriteLine($"Task Handler Service: Consumed {taskId}");
                    await Task.Delay(200);
                    await redisClient.SetAsync<string>(taskId, "in_progress",
                        cancelToken);

                    await Task.Delay(30000, cancelToken);

                    await redisClient.SetAsync<string>(taskId, "done",
                        cancelToken);
                }
            }
            catch (OperationCanceledException)
            {
                consumerBuilder.Close();
                this.logger.LogInformation("Cancel invoked! Shutting down");
            }
        }
        return;
    }

    public Task StopAsync(CancellationToken cancellationToken)
    {
        return Task.CompletedTask;
    }
}