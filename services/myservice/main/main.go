package main

import (
	"errors"
	"fmt"
	"io"
	"net/http"
	"os"
	"post"
	"time"

	"go.uber.org/zap"
)

var logger *zap.Logger
var sugar *zap.SugaredLogger

func measureTime(f func(http.ResponseWriter, *http.Request)) func(http.ResponseWriter, *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		t0 := time.Now()
		defer func() {
			if err := recover(); err != nil {
				sugar.Errorln("panic occurred:" + err.(string))
			}
		}()
		defer sugar.Info("execution time: ", time.Since(t0).Seconds())
		f(w, r)
	}
}

func getRoot(w http.ResponseWriter, r *http.Request) {
	sugar.Info("got / request\n")
	ctx := r.Context()
	select {
	case <-time.After(2 * time.Second):
		io.WriteString(w, "Root request success\n")
	case <-ctx.Done():
		err := ctx.Err()
		sugar.Errorln(err)
		http.Error(
			w, err.Error(),
			http.StatusInternalServerError,
		)
	}
}

func getLastPost(w http.ResponseWriter, r *http.Request) {
	sugar.Info("got /lastpost request\n")
	ctx := r.Context()
	select {
	case <-time.After(2 * time.Second):
		io.WriteString(w, post.LastPost())
	case <-ctx.Done():
		err := ctx.Err()
		sugar.Errorln(err)
		http.Error(
			w, err.Error(),
			http.StatusInternalServerError,
		)
	}
}

func main() {
	http.HandleFunc("/", measureTime(getRoot))
	http.HandleFunc("/lastpost", measureTime(getLastPost))

	logger, _ = zap.NewProduction()
	defer logger.Sync()
	sugar = logger.Sugar()

	sugar.Info("Starting server!")
	err := http.ListenAndServe(":3333", nil)
	if errors.Is(err, http.ErrServerClosed) {
		fmt.Printf("server closed\n")
	} else if err != nil {
		fmt.Printf("error starting server: %s\n", err)
		os.Exit(1)
	}
}
