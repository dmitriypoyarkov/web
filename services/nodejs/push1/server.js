const port = 9494,
      mail = "a",
      publicKey = "BELZnqH_kahteeD_93MWfXF0p-DZykDW3YMVY5jPDgRfRZUs1JE1jnPdRJ2CSs7XLM49Rf6KKodtqYUaGazSvlo",
      privateKey = "0azfYtcHexbvvyZ2KbkXin-ei2-a6sqlga4WFurT8Lw";

const express = require("express"),
      bodyParser = require("body-parser"),
      path = require("path"),
      webpush = require("web-push");

webpush.setVapidDetails("mailto:" + mail, publicKey, privateKey);
const app = express();
app.use(express.static(__dirname));
app.use(bodyParser.json());

app.get("/", (req, res) => {
  res.sendFile(path.join(__dirname, "/page.html"));
});

app.post("/sendpush", (req, res) => {
  res.status(201).json({}); // REPLY WITH 201 (CREATED)
  webpush.sendNotification(req.body, JSON.stringify({
    title: "Push",
    body: "Test"
  }))
  .catch((err) => { console.log(err); });
});

app.listen(port, () => {
  console.log(`Listening on ${port}`)
});
