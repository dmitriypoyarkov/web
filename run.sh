# build go server
cd ./services/myservice
sudo docker build --tag docker-gs-myservice .
cd ../..
#build rust services
cd ./services/rust_ws
sudo docker build --tag rust-services .
cd ../..
# Run
sudo docker compose up --remove-orphans