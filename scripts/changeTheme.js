
function waitForServer() {
    return new Promise(r => setTimeout(r, 3000));
 } 

async function requestToServer() {
    console.log('Waiting for server...');
    await waitForServer();
    console.log('Waiting done.')
    if (Math.random() > 0.5) {
        return true;
    }
    return false;
}

var themes = ['light', 'dark'];
var currentThemeIndex = 1;
// Функция как-будто делает запрос к серверу и
// на основании результата либо меняет тему, либо
// нет.
export async function tryChangeTheme() {
    let serverResponse = await requestToServer();
    if (serverResponse == false) {
        throw 'Server connection error';
    }
    currentThemeIndex = (currentThemeIndex + 1) % themes.length;
    var newTheme = themes[currentThemeIndex];
    document.documentElement.setAttribute("data-theme",
        newTheme);
}

var fonts = ['arial', 'times'];
var currentFontIndex = 0;
export function changeFont() {
    currentFontIndex = (currentFontIndex + 1) % fonts.length;
    var newFont = fonts[currentFontIndex];
    document.documentElement.setAttribute("data-font",
        newFont);
}

